package com.kiss.formation.cassandra.simplecql;

import com.datastax.driver.core.*;
import com.kiss.formation.cassandra.connect.CassandraConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Component
public class PreparedStatementExample {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CassandraConnection cassandraConnection;
    private PreparedStatement insertPreparedStatement;
    private PreparedStatement selectPreparedStatement;
    private Session session;

    @PostConstruct
    public void initialize() {
        // Insert simple video

        session = cassandraConnection.getSession();
        insertPreparedStatement = session.prepare("INSERT INTO video(id, name) VALUES (? , ?)");
        selectPreparedStatement = session.prepare("SELECT * FROM VIDEO WHERE id = ?");

        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        insert(uuid1, "PreparedStatement video1");
        insert(uuid2, "PreparedStatement video2");

        read(uuid1);
        read(uuid2);
    }

    private void insert(UUID id, String name) {
        BoundStatement boundStatement = insertPreparedStatement.bind(id, name);
        ResultSet execute = session.execute(boundStatement);
        if (execute.wasApplied()) {
            logger.info("video added: " + id);
        }
    }

    private void read(UUID id) {
        BoundStatement boundStatement = selectPreparedStatement.bind(id);
        ResultSet resultSet = session.execute(boundStatement);
        Row row = resultSet.one();
        if (row != null) {
            logger.info("Found video with id: {}, name: {}", id, row.getString("name"));
        }
    }

}
