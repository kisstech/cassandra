package com.kiss.formation.cassandra.simplecql;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.kiss.formation.cassandra.connect.CassandraConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Component
public class SimpleExample {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CassandraConnection cassandraConnection;

    @PostConstruct
    public void initialize() {
        // Insert simple video
        UUID uuid = UUID.randomUUID();
        ResultSet insertResultSet = cassandraConnection.getSession().execute("INSERT INTO video(id, name) VALUES (" + uuid + ", 'Ma première vidéo')");
        if (insertResultSet.wasApplied()) {
            logger.info("Video added" + uuid);

            ResultSet execute = cassandraConnection.getSession().execute("SELECT * FROM VIDEO WHERE id = " + uuid.toString());
            Row row = execute.one();
            if (row != null) {
                logger.info("Found video with id: {}, name: {}", uuid, row.getString("name"));
            }
        }
    }

}
