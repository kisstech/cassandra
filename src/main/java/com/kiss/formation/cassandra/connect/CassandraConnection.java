package com.kiss.formation.cassandra.connect;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class CassandraConnection {

    private Session session;

    @PostConstruct
    public void initialize() {
        Cluster cluster = Cluster.builder()
                .addContactPoint("127.0.0.1")
                .build();
        session = cluster.connect("videotube");
    }

    public Session getSession() {
        return session;
    }
}
